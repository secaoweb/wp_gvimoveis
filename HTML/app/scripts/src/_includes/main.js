$(".busca-avancada").click(function(){
  $("#form-avancado").toggle(function(){
    $(this).animate({height:40},200);
  },function(){
    $(this).animate({height:'auto'},200);
  });
})

$('#slide-imovel').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  navigation: true,
  adaptiveHeight: true
});

$('#banner-home').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  navigation: true,
});
