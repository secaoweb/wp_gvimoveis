<?php
/**
 * Property Default Template for Single Property View
 *
 * Overwrite by creating your own in the theme directory called either:
 * property.php
 * or add the property type to the end to customize further, example:
 * property-building.php or property-floorplan.php, etc.
 *
 * By default the system will look for file with property type suffix first,
 * if none found, will default to: property.php
 *
 * Copyright 2010 Andy Potanin <andy.potanin@twincitiestech.com>
 *
 * @version 1.3
 * @author Andy Potanin <andy.potnain@twincitiestech.com>
 * @package WP-Property
 */

// Uncomment to disable fancybox script being loaded on this page
//wp_deregister_script('wpp-jquery-fancybox');
//wp_deregister_script('wpp-jquery-fancybox-css');
?>

<?php get_header(); ?>
<?php the_post(); ?>

    <script type="text/javascript">
        var map;
        var marker;
        var infowindow;

        jQuery(document).ready(function () {

            if (typeof jQuery.fn.fancybox == 'function') {
                jQuery("a.fancybox_image, .gallery-item a").fancybox({
                    'transitionIn': 'elastic',
                    'transitionOut': 'elastic',
                    'speedIn': 600,
                    'speedOut': 200,
                    'overlayShow': false
                });
            }

            if (typeof google == 'object') {
                initialize_this_map();
            } else {
                jQuery("#property_map").hide();
            }

        });


        function initialize_this_map() {
            <?php if($coords = WPP_F::get_coordinates()): ?>
                var myLatlng = new google.maps.LatLng(<?php echo $coords['latitude']; ?>, <?php echo $coords['longitude']; ?>);
                var myOptions = {
                    zoom: <?php echo (!empty($wp_properties['configuration']['gm_zoom_level']) ? $wp_properties['configuration']['gm_zoom_level'] : 13); ?>,
                    center: myLatlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("property_map"), myOptions);

                infowindow = new google.maps.InfoWindow({
                    // TODO conteudo padrao do plugin => content: '<?php //echo WPP_F::google_maps_infobox($post); ?>',
                    content: '<?php echo format_content_map_property($property); ?>',
                    maxWidth: 500
                });

                marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: '<?php echo addslashes($post->post_title); ?>',
                    icon: '<?php echo apply_filters('wpp_supermap_marker', '', $post->ID); ?>'
                });

                setTimeout("infowindow.open(map,marker);", 1000);
            <?php endif; ?>
        }
    </script>

    <section class="container">
        <div class="row">
            <main id="main-content" class="col-md-9">
                <h1 class="title-imovel">
                    <?php the_title(); ?>
                    <span class="label <?php echo ($property['tipo_de_negociao'] == "Venda") ? 'label-danger' : 'label-warning'; ?> pull-right"><?php echo $property['tipo_de_negociao']; ?></span>
                </h1>

                <div id="slide-imovel">
                    <?php if(!empty($property)) foreach($property['gallery'] as $image): ?>
                        <div class="item-slide">
                            <img src="<?php echo $image['medium_large'] ?>" alt="<?php the_title(); ?>">
                        </div>
                    <?php endforeach; ?>
                </div>

                <?php
                $labels = [];
                $values = [];
                $attributes = draw_stats(['display' => 'array']);

                if(!empty($attributes)) {
                    $exclude_attributes = ['Endereço', 'Tipo de Negociação', 'Cidade', 'Bairro'];

                    foreach ($attributes as $attribute) {
                        if (in_array($attribute['label'], $exclude_attributes)) {
                            continue;
                        }

                        $labels[] = $attribute['label'];
                        $values[] = $attribute['value'];
                    }
                }
                ?>

                <div class="footer-imovel single-imovel-footer hidden-xs">
                    <?php if(!empty($property)): ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <?php foreach ($labels as $label): ?>
                                        <th><?php echo $label; ?></th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php foreach ($values as $value): ?>
                                        <td><?php echo $value; ?></td>
                                    <?php endforeach; ?>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>

                <div class="footer-imovel single-imovel-footer visible-xs" style="font-size: 12px">
                    <?php if(!empty($property)): ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <?php foreach ($labels as $key => $label): ?>
                                        <?php if ($key < 4): ?><th><?php  echo $label; ?></th><?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php foreach ($values as $key => $value): ?>
                                        <?php if ($key < 4): ?><td><?php  echo $value; ?></td><?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table">
                            <thead>
                                <tr>
                                    <?php foreach ($labels as $key => $label): ?>
                                        <?php if ($key > 3): ?><th><?php echo $label; ?></th><?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <?php foreach ($values as $key => $value): ?>
                                        <?php if ($key > 3): ?><td><?php  echo $value; ?></td><?php endif; ?>
                                    <?php endforeach; ?>
                                </tr>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>

                <div class="box-info">
                    <!-- <div class="info">Detalhes do Imóvel</div> -->

                    <div class="content-box">
                        <p><?php the_content(); ?></p>
                        <p><?php if ($property['formatted_address']) { echo '<hr /><strong>Endereço: </strong>'.$property['formatted_address']; } ?></p>
                    </div>
                </div>

                <div class="box-info">
                    <!-- <div class="info">Localização do Imóvel</div> -->
                    <div class="content-box">
                        <?php if (WPP_F::get_coordinates()): ?>
                            <div id="property_map" class="<?php wpp_css('property::property_map'); ?>" style="width:100%; height:300px"></div>
                        <?php endif; ?>
                    </div>
                </div>
            </main>

            <?php get_sidebar('property-post'); ?>
        </div>
    </section>

<?php get_footer(); ?>
