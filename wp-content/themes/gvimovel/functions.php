<?php
// theme options
require('theme-options.php');

// Add RSS links to <head> section
automatic_feed_links();

// Clean up the <head>
function removeHeadLinks()
{
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
}

add_action('init', 'removeHeadLinks');
remove_action('wp_head', 'wp_generator');

// Adicionar suporte ao resumo nas páginas
add_post_type_support('page','excerpt');

// Adicionar a função de imagem destacada
add_theme_support("post-thumbnails");
if (function_exists('the_post_thumbnail')) {
    the_post_thumbnail();
}

// Adicionar Template para página Single (múltipla singles)
add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-{$cat->slug}.php"; } return $t;'));

// Declaração da sidebar
if (function_exists ('register_sidebar')) {
    // nova sidebar para listagem de imóveis
    register_sidebar('property');
}

// Converter texto para maísculo
function toUppercase($string){
    if(empty($string)){
        return null;
    }

    return strtr(strtoupper($string), "àáâãäåæçèéêëìíîïðñòóôõö÷øùüúþÿ", "ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÜÚÞß");
}

// MENU PERSONALIZADO
function sw_nav_menu($menu_name)
{
    $args = array(
        'order' => 'ASC',
        'orderby' => 'menu_order',
        'post_type' => 'nav_menu_item',
        'post_status' => 'publish',
        'output' => ARRAY_A,
        'output_key' => 'menu_order',
        'nopaging' => true,
        'update_post_term_cache' => false
    );

    $url_atual = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $menu_itens = wp_get_nav_menu_items($menu_name, $args);
    $menu_html = "";

    //Início do Menu.
    $menu_html .= '<div class="collapse navbar-collapse" id="navbar"><ul class="nav navbar-nav">';

    foreach ($menu_itens as $item):
        $class_active = '';
        $url = $item->url;

        if ($url === $url_atual) {
            $class_active = 'class="active"';
        }

        $menu_html .= '<li '.$class_active.'><a href="'.$url.'">'.$item->title.'</a></li>';
    endforeach;

    $menu_html .= '</ul>
            <ul class="nav navbar-nav navbar-right hidden-md">
                <li>
                  <ul class="call-now">
                    <li>Ligue agora!</li>
                  </ul>
                  <a href="#">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-whatsapp fa-stack-1x"></i>
                    </span>
                    '.gv_get_theme_option('whatsapp').'
                  </a>
                </li>
                <li>
                  <a href="#">
                    <span class="fa-stack fa-lg">
                      <i class="fa fa-phone fa-stack-1x"></i>
                    </span>
                    '.gv_get_theme_option('telefone').'
                  </a>
                </li>
            </ul>
        </div><!-- navbar-collapse -->';
    //Fim do Menu.

    return $menu_html;
}

// Paginação personalizada
function sw_pagination($pages = '', $range = 4) {
    $pagination_html = '';
    $showitems = ($range * 2) + 1;
    global $paged;

    if (empty($paged)) {
        $paged = 1;
    }

    if ($pages == '') {
        global $wp_query;
        $pages = $wp_query->max_num_pages;

        if (!$pages) {
            $pages = 1;
        }
    }

    if (1 != $pages) {
        $pagination_html .= '<ul class="pagination pull-right">';

        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) {
            $pagination_html .=  "<li><a href='" . get_pagenum_link($paged - 1) . "'>&laquo;</a></li>";
        }

        if ($paged > 6 && $showitems < $pages) {
            $pagination_html .=  "<li><a href='" . get_pagenum_link(1) . "'>1</a></li><li><a href='#'>...</a></li>";
        }

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems )) {
                $pagination_html .=  ($paged == $i) ? "<li><a href='#'>" . $i . "</li>" : "<li><a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a></li>";
            }
        }

        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages) {
            $pagination_html .=  "<li><a href='#'>...</a></li><li><a href='" . get_pagenum_link($pages) . "'>$pages</a></li>";
        }

        if ($paged < $pages && $showitems < $pages) {
            $pagination_html .=  "<li><a href='" . get_pagenum_link($paged + 1) . "'>&raquo;</a></li>";
        }

        $pagination_html .=  "</ul>";

        return $pagination_html;
    }
}

// alterar conteudo padrao da caixa de informacao do mapa
if (!function_exists('format_content_map_property')) {
    function format_content_map_property($property = []) {
        if ($property != []) {

            $img_src = !empty($property['images'])? $property['images']['map_thumb'] : '';
            $html = '<div style="text-align: center;">' .
                '<span style="color: darkgray;font-weight: bold;">' . $property['post_title'] .'</span><br/>' .
                '<img style="border: 3px solid #f8f8f8;opacity:0.7;border-radius:3px;" src="' . $img_src .'" alt="' . $property['post_title'] .'" />' .
                '</div>';

            return $html;
        }
    }
}

// pegar os valores dos atributos dos imóveis
if (!function_exists('get_attribute_values')) {
    function get_attribute_values($attr) {
        global $wpdb;

        $query = 'SELECT meta_value
            FROM '.$wpdb->prefix.'postmeta
            WHERE meta_key = "'.$attr.'"
            GROUP BY meta_value';

        $items = $wpdb->get_results($query);
        return $items;
    }
}

?>
