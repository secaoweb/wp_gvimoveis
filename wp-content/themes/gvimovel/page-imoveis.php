<?php
get_header();

the_post();

?>
<section id="main-section-content" class="container">
    <div class="row">
        <main id="main-content" class="col-md-9 listagem">

            <?php echo do_shortcode( '[property_overview per_page='. get_option('posts_per_page') .' pagination=on pagination_type=numeric ]' ); ?>

        </main>

        <?php get_sidebar('property'); ?>

    </div>
</section>
<?php

get_footer();

?>
