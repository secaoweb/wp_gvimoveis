<?php
get_header();

the_post();
?>

    <section id="contato-page" class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1 class="title-page"><?php the_title(); ?></h1>

                <p class="content-page"><?php the_content(); ?></p>

                <?php echo do_shortcode('[contact-form-7 id="30" title="Contato"]'); ?>
            </div>
        </div>

    </section>

<?php get_footer(); ?>
