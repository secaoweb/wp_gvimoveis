<?php
if (have_properties()):
    foreach (returned_properties('load_gallery=false') as $property):
        ?>

        <div class="item-listagem row row-no-padding">
            <div class="col-md-5">
                <div class="imagem-imovel">
                    <div class="image-container">
                        <a href="<?php echo $property['permalink']; ?>">
                            <img class="hidden-sm" src="<?php echo $property['images']['thumb_featured']; ?>" alt="<?php echo $property['title']; ?>">
                            <img class="visible-sm" src="<?php echo $property['images']['medium_large']; ?>" alt="<?php echo $property['title']; ?>">
                        </a>
                    </div>
                    <div class="overlay-imovel">
                        <div class="area"><?php echo $property['area']; ?></div>
                        <div class="preco"><?php echo $property['price']; ?></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-7 info-border">
                <div class="info-imovel">
                    <a href="<?php echo $property['permalink']; ?>">
                        <h1>
                            <?php echo $property['post_title']; ?>
                            <small><span class="label <?php echo ($property['tipo_de_negociao'] == "Venda") ? 'label-danger' : 'label-warning'; ?> pull-right"><?php echo $property['tipo_de_negociao']; ?></span></small>
                        </h1>
                    </a>
                    <p><?php echo $property['formatted_address'] ?></p>
                    <p class="visible-lg"><?php echo wp_trim_words($property['post_content'], 40); ?></p>
                    <p class="visible-md"><?php echo wp_trim_words($property['post_content'], 25); ?></p>
                    <p class="visible-sm"><?php echo wp_trim_words($property['post_content'], 20); ?></p>
                    <p class="visible-xs"><?php echo wp_trim_words($property['post_content'], 15); ?></p>
                    <a href="<?php echo $property['permalink']; ?>" class="btn btn-light-blue-radius">Mais informações</a>
                </div>
                <div class="footer-imovel">
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-bed fa-stack-1x"></i>
                    </span>
                    <?php echo sprintf(_n('%s Quarto', '%s Quartos', $property['quartos']), $property['quartos']); ?>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-bath fa-stack-1x"></i>
                    </span>
                    <?php echo sprintf(_n('%s Banheiro', '%s Banheiros', $property['banheiros']), $property['banheiros']); ?>
                    <span class="fa-stack fa-lg">
                        <i class="fa fa-car fa-stack-1x"></i>
                    </span>
                    <?php echo sprintf(_n('%s Vaga', '%s Vagas', $property['vagas']), $property['vagas']); ?>
                </div>
            </div>
        </div>

        <?php /** end of the propertyloop. */
    endforeach;
else:
    echo '<h3 style="padding-left: 15px;margin-top: 0px;">Nenhum imóvel encontrado.</h3>';
endif;
?>
