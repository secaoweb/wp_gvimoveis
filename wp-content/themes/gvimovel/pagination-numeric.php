<?php
/**
 * Property Overview Pagination
 * Pagination Type: 'Numeric'
 *
 * To modify template, copy the file to root of your theme.
 */

$requested_page = 2;
$action = 'wpp_property_overview_pagination';
$wpp_ajax_query = $wpp_query;

$wpp_ajax_query['template'] = true;
$wpp_ajax_query['ajax_call'] = true;
$wpp_ajax_query['disable_wrapper'] = true;
$wpp_ajax_query['index'] = 1;

?>
<script>

    /* ############################## Função principal de consultas [Paginação] ################################### */

    function properties_pagination(page_event) {

        wpp_ajax_query_data = <?php echo json_encode(['action' => $action, 'wpp_ajax_query' => $wpp_ajax_query]); ?>;

        var data_query = wpp_ajax_query_data;

        // se a página anterior foi requisitada
        if (page_event == page_event_prev) {
            if (current_page > 1) {
                start_block_default();

                current_page--;
                data_query['wpp_ajax_query']['query']['requested_page'] = current_page;
                data_query['wpp_ajax_query']['requested_page'] = current_page;
            } else {
                return false;// já está na página "mínima"
            }
        }
        // se a próxima página foi requisitada
        else if (page_event == page_event_next) {
            if (current_page < data_query['wpp_ajax_query']['pages']) {
                start_block_default();

                current_page++;
                data_query['wpp_ajax_query']['query']['requested_page'] = current_page
                data_query['wpp_ajax_query']['requested_page'] = current_page;
            } else {
                return false;// já está na página "máxima"
            }
        }

        // definir default_query
        data_query['wpp_ajax_query']['default_query'] = data_query['wpp_ajax_query']['query'];

        $.ajax({
            method: "POST",
            url: "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php",
            data: data_query,
            cache: false,
            dataType: "json",
            success: function (data) {

                wpp_ajax_query_data = {
                    'action' : '<?php echo $action; ?>', 'wpp_ajax_query' : data['wpp_query']
                };
                // exibir conteúdo do resultado
                var section = $('#wpp_shortcode_' + wpp_ajax_query_data['wpp_ajax_query']['unique_hash']);
                section.empty();
                section.html(data['display']).fadeIn('slow');

                /**
                 * Informar marcadores de requisição de páginas
                 */
                if (page_event == page_event_prev) {
                    requested_page_prev--;
                    requested_page_next--;
                } else {
                    requested_page_prev++;
                    requested_page_next++;
                }

                $('html, body').animate({
                    scrollTop: $('#wpp_shortcode_' + wpp_ajax_query_data['wpp_ajax_query']['unique_hash']).offset().top
                }, 700);

                window.location.replace("#/page/"+current_page);
            },
            error: function () { console.log('Error no evento de paginacao...'); }
        });

        finish_block_default();
    }

    /* ############################## Função principal de ordenação ############################################### */

    function properties_sort_by(sort_by) {

        wpp_ajax_query_data = <?php echo json_encode(['action' => $action, 'wpp_ajax_query' => $wpp_ajax_query]); ?>;
        // Verificar se parâmetro é válido
        var sortable_attr_exists = function (sort_by) {
            var sortable_attrs = wpp_ajax_query_data['wpp_ajax_query']['sortable_attrs'];
            if (sort_by == '') {
                return false;
            }
            return sortable_attrs[sort_by] != undefined;
        };

        if (!sortable_attr_exists(sort_by) && sort_by != 'post_date') {
            return false;
        }

        start_block_default();

        // valores padroes para requisições "sort"
        var order_asc = $('#radio-properties-sort-order-asc'),
            order_desc = $('#radio-properties-sort-order-desc'),
            sort_order = (order_asc.prop('checked'))? order_asc.val() : order_desc.val();

        if (wpp_ajax_query_data['wpp_ajax_query']['default_query'] == undefined) {
            wpp_ajax_query_data['wpp_ajax_query']['default_query'] = wpp_ajax_query_data['wpp_ajax_query']['query'];
        }

        wpp_ajax_query_data['wpp_ajax_query']['sort_order'] = sort_order;
        wpp_ajax_query_data['wpp_ajax_query']['query']['sort_order'] = sort_order;
        wpp_ajax_query_data['wpp_ajax_query']['default_query']['sort_order'] = sort_order;
        wpp_ajax_query_data['wpp_ajax_query']['requested_page'] = wpp_ajax_query_data['wpp_ajax_query']['current_page'];
        wpp_ajax_query_data['wpp_ajax_query']['query']['requested_page'] = wpp_ajax_query_data['wpp_ajax_query']['current_page'];
        wpp_ajax_query_data['wpp_ajax_query']['query']['sort_by'] = sort_by;
        wpp_ajax_query_data['wpp_ajax_query']['sort_by'] = sort_by;
        wpp_ajax_query_data['wpp_ajax_query']['default_query']['sort_by'] = sort_by;
        wpp_ajax_query_data['wpp_ajax_query']['default_query']['requested_page'] = wpp_ajax_query_data['wpp_ajax_query']['requested_page'];

        $.ajax({
            method: "POST",
            url: "<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php",
            data: wpp_ajax_query_data,
            cache: false,
            dataType: "json",
            success: function (data) {

                wpp_ajax_query_data = {
                    'action' : '<?php echo $action; ?>', 'wpp_ajax_query' : data['wpp_query']
                };
                // exibir conteúdo do resultado
                var section = $('#wpp_shortcode_' + wpp_ajax_query_data['wpp_ajax_query']['unique_hash']);
                section.empty();
                section.html(data['display']).fadeIn('slow');

                $('html, body').animate({
                    scrollTop: $('#wpp_shortcode_' + wpp_ajax_query_data['wpp_ajax_query']['unique_hash']).offset().top
                }, 700);
            },
            error: function () { console.log('Error no evento de ordenação...'); }
        });

        finish_block_default();
    }

    // evento do select de ordenação
    function select_properties_sort() {
        var option_selected = $('.select-properties-sort').val();

        if (option_selected == '') {
            properties_sort_by('post_date');
        } else {
            properties_sort_by(option_selected);
        }
    }

</script>

<?php

$current_page = (int) $wpp_query['current_page'];
$pages = (int) $wpp_query['pages'];

?>

<div class="row paginator">
    <main class="col-md-9 listagem">
        <nav aria-label="...">
            <ul class="pager">
                <li <?php if($current_page == 1): echo 'class="disabled" style="opacity: 0.5"'; endif; ?>>
                    <a href="#" onclick="event.preventDefault(); properties_pagination('prev');  return false;">
                        <span aria-hidden="true">&larr;</span> <?php _e('Prev', ud_get_wp_property()->domain); ?>
                    </a>
                </li>
                <li <?php if($current_page == $pages): echo 'class="disabled" style="opacity: 0.5"'; endif; ?>>
                    <a href="#" onclick="event.preventDefault(); properties_pagination('next'); return false;">
                        <?php _e('Next', ud_get_wp_property()->domain); ?> <span aria-hidden="true">&rarr;</span>
                    </a>
                </li>
            </ul>
        </nav>
    </main>
</div>