<?php
/**
 * WP-Property Overview Template
 *
 * To customize this file, copy it into your theme directory, and the plugin will
 * automatically load your version.
 *
 * You can also customize it based on property type.  For example, to create a custom
 * overview page for 'building' property type, create a file called property-overview-building.php
 * into your theme directory.
 *
 *
 * Settings passed via shortcode:
 * $properties: either array of properties or false
 * $show_children: default true
 * $thumbnail_size: slug of thumbnail to use for overview page
 * $thumbnail_sizes: array of image dimensions for the thumbnail_size type
 * $fancybox_preview: default loaded from configuration
 * $child_properties_title: default "Floor plans at location:"
 *
 *
 *
 * @version 1.4
 * @author Andy Potanin <andy.potnain@twincitiestech.com>
 * @package WP-Property
 */ ?>

<h1 class="info">
    <?php
    $count_properties = count($properties);
    echo sprintf(_n('%s imóvel encontrado', '%s imóveis encontrados', $count_properties), $count_properties);
    ?>
    <a href="#" class="visible-xs visible-sm btn btn-sm btn-primary pull-right" style="margin-top: -5px" onclick="run_box_sidebar('filter')">
        <span class="sort-order-description">pesquisar</span> <i class="fa fa-search"></i>
    </a>
</h1>

<div class="row ordenar">
    <div class="col-md-12">
        <div class="form-group col-md-5">
            <label class="label-text">Ordenar por: </label>
            <label class="label-radio-order">
                <input id="radio-properties-sort-order-asc" class="radio-properties-sort-order" name="order"
                       value="ASC" type="radio" <?php echo $wpp_query['sort_order'] == 'ASC' ? 'checked' : ''; ?>
                       onclick="if(document.getElementById('select-properties-sort').value != ''){select_properties_sort()}"/> &darr; <span class="sort-order-description">menor</span>
            </label>
            <label class="label-radio-order">
                <input id="radio-properties-sort-order-desc" class="radio-properties-sort-order" name="order"
                       value="DESC" type="radio" <?php echo $wpp_query['sort_order'] == 'DESC' ? 'checked' : ''; ?>
                       onclick="if(document.getElementById('select-properties-sort').value != ''){select_properties_sort()}"/> &uarr; <span class="sort-order-description">maior</span>
            </label>
            <label class="select">
                <select id="select-properties-sort" class="form-control select-properties-sort"
                        onchange="select_properties_sort();">
                    <?php
                    echo '<option value=""> Selecione... </option>';

                    foreach ($wpp_query['sortable_attrs'] as $attr => $value):
                        $selected = $wpp_query['sort_by'] == $attr ? 'selected' : '';
                        echo '<option ' . $selected . ' value="' . $attr . '"> ' . $value . ' </option>';
                    endforeach;
                    ?>
                </select>
            </label>
        </div>
    </div>
</div>

<?php

get_template_part('loop', 'property');

include self::get_pagination_template_based_on_type('numeric');

?>
