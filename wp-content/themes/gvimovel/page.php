<?php
get_header();

the_post();
?>

<section id="contato-page" class="container" style="margin-bottom: 20px;">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="title-page"><?php the_title(); ?></h1>

            <p class="content-page"><?php the_content(); ?></p>
        </div>
    </div>
</section>

<?php get_footer(); ?>
