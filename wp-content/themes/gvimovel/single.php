<?php
get_header();

$category = get_the_category();
$category_link = get_category_link($category[0]->cat_ID);
?>

<section class="breadcrumb-blog">
    <div class="container">
        <div class="pull-left"><h1><?php echo $category[0]->cat_name; ?></h1></div>
        <div class="pull-right path-bread">
            <a href="<?php echo home_url(); ?>">Home</a> /
            <span><a href="<?php echo $category_link ?>"><?php echo $category[0]->cat_name; ?></a></span>
        </div>
    </div>
</section>

<section class="container" id="body-blog">
    <div class="row">
        <main class="col-md-9">
            <?php get_template_part('loop', 'single'); ?>
            <span class="clearfix"></span>

            <?php comments_template('', true); ?>
        </main>

        <aside class="col-md-3">
            <?php dynamic_sidebar('main'); ?>
        </aside>
    </div><!-- ROW -->
</section>

<?php get_footer(); ?>
