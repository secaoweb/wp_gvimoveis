<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to twentyeleven_comment() which is
 * located in the functions.php file.
 */
?>

<div id="comments">
    <h3 class="headInModule postcomment"><?php echo comments_number('Sem comentários', '1 comentário', '% comentários'); ?>:</h3>

    <?php
    if (have_comments()):
        echo '<ol class="commentlist">';
        foreach ($comments as $comment):
            ?>
            <div class="about-author">
                <div class="author-image">
                    <?php echo get_avatar($comment, $size = '80', $default = ''); ?>
                </div>
                <div class="author-details">
                    <h3><?php printf(__('%s'), get_comment_author_link()); ?></h3>
                    <?php comment_text(); ?>
                </div> 
            </div>
            <hr />
            <?php
        endforeach;
        echo '</ol>';
    else:
        echo '<p>Nenhum comentário foi feito ainda, seja o primeiro!</p><hr>';
    endif;
    ?>
</div><!-- #comments --> 

<div class="comments">  
    <?php if (comments_open()):
        ?>
        <div id="respond">
            <div class="bg_title"><h3 id="reply-title" style="text-align: center;" class="headInModule postcomment">Deixe seu comentário: <small><a style="display:none;" href="#respond" id="cancel-comment-reply-link" rel="nofollow">Cancel resposta</a></small></h3></div>

            <form class="form-horizontal" id="commentform" method="post" action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php">
                <div class="control-group">
                    <label class="control-label">Nome:</label>
                    <div class="controls">
                        <input type="text" name="author" id="author" title="Name" placeholder="Nome"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Email:</label>
                    <div class="controls">
                        <input type="text" name="email" id="email" title="Email" placeholder="Email"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Comentário:</label>
                    <div class="controls">
                        <textarea rows="5" cols="45" name="comment" placeholder="Comentário"></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <button type="submit" id="submit" name="submit" class="btn">Comentar</button>
                    </div>
                </div>

                <?php comment_id_fields(); ?>
                <?php do_action('comment_form', $post->ID); ?>
            </form>
        </div>
        <?php
    else:
        echo '<p>Não são permitidos comentários para essa postagem.</p>';
    endif;
    ?>

    <br class="clear" />
</div>