<?php
/**
 * Create Theme Options Panel
 *
 */

// Exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

// Start Class
if (!class_exists('WPEX_Theme_Options')) {

    class WPEX_Theme_Options {
        /**
         * Start things up
         *
         * @since 1.0.0
         */
        public function __construct() {
            // We only need to register the admin panel on the back-end
            if (is_admin()) {
                add_action('admin_menu', array('WPEX_Theme_Options', 'add_admin_menu'));
                add_action('admin_init', array('WPEX_Theme_Options', 'register_settings'));
            }

        }

        /**
         * Returns all theme options
         *
         * @since 1.0.0
         */
        public static function get_theme_options() {
            return get_option('theme_options');
        }

        /**
         * Returns single theme option
         *
         * @since 1.0.0
         */
        public static function get_theme_option($id) {
            $options = self::get_theme_options();
            if (isset($options[$id])) {
                return $options[$id];
            }
        }

        /**
         * Add sub menu page
         *
         * @since 1.0.0
         */
        public static function add_admin_menu() {
            add_menu_page(
                esc_html__('Opções do Tema', 'text-domain'),
                esc_html__('Opções do Tema', 'text-domain'),
                'manage_options',
                'theme-settings',
                array('WPEX_Theme_Options', 'create_admin_page')
            );
        }

        /**
         * Register a setting and its sanitization callback.
         *
         * We are only registering 1 setting so we can store all options in a single option as
         * an array. You could, however, register a new setting for each option
         *
         * @since 1.0.0
         */
        public static function register_settings() {
            register_setting('theme_options', 'theme_options', array('WPEX_Theme_Options', 'sanitize'));
        }

        /**
         * Sanitization callback
         *
         * @since 1.0.0
         */
        public static function sanitize($options) {
            // If we have options lets sanitize them
            if ($options) {
                foreach ($options as $key => $value) {
                    if (!empty($value)) {
                        $options[$key] = sanitize_text_field($value);
                    } else {
                        unset($options[$key]); // Remove from options if empty
                    }
                }
            }

            // Return sanitized options
            return $options;
        }

        /**
         * Settings page output
         *
         * @since 1.0.0
         */
        public static function create_admin_page() { ?>
            <style type="text/css">
                input[type="text"], textarea { width: 100%; }
                textarea { height: 100px; }
            </style>

            <div class="wrap">
                <h1><?php esc_html_e('Opções do Tema', 'text-domain' ); ?></h1>

                <form method="post" action="options.php">
                    <?php settings_fields( 'theme_options' ); ?>

                    <h2>Contatos</h2>
                    <table class="form-table wpex-custom-admin-login-table">
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e('Whatsapp', 'text-domain'); ?></th>
                            <td>
                                <?php $value = self::get_theme_option('whatsapp'); ?>
                                <input type="text" name="theme_options[whatsapp]" value="<?php echo esc_attr($value); ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e('Telefone', 'text-domain'); ?></th>
                            <td>
                                <?php $value = self::get_theme_option('telefone'); ?>
                                <input type="text" name="theme_options[telefone]" value="<?php echo esc_attr($value); ?>">
                            </td>
                        </tr>
                        <tr valign="top">
                            <th scope="row"><?php esc_html_e('E-mail', 'text-domain'); ?></th>
                            <td>
                                <?php $value = self::get_theme_option('email'); ?>
                                <input type="text" name="theme_options[email]" value="<?php echo esc_attr($value); ?>">
                            </td>
                        </tr>
                    </table>

                    <?php submit_button(); ?>
                </form>
            </div><!-- .wrap -->
        <?php }
    }
}
new WPEX_Theme_Options();

// Helper function to use in your theme to return a theme option value
function gv_get_theme_option($id = '') {
    return WPEX_Theme_Options::get_theme_option($id);
}
