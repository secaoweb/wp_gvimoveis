<?php get_header(); ?>

<section class="breadcrumb-blog">
    <div class="container">
        <div class="pull-left"><h1>Tags</h1></div>
    </div>
</section>

<section class="container" id="body-blog">
    <div class="row">
        <main class="col-md-9">
            <article class="item-post">
                <?php get_template_part('loop', 'category'); ?>
            </article><!-- ITEM POST -->

            <nav aria-label="Page navigation" class="clearfix">
                <?php echo sw_pagination(); ?>
            </nav>
        </main>

        <aside class="col-md-3">
            <?php dynamic_sidebar('main'); ?>

            <!-- <div class="facebook">
                <h1>Facebook</h1>

                <div class="fb-page" data-height="350" data-href="https://www.facebook.com/ejudi.ufc" data-tabs="timeline" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <blockquote cite="https://www.facebook.com/ejudi.ufc" class="fb-xfbml-parse-ignore">
                        <a href="https://www.facebook.com/ejudi.ufc">EJUDI</a>
                    </blockquote>
                </div>
            </div> -->

            <!-- <div class="instagram">
                <h1>Instagram</h1>
            </div> -->
        </aside>
    </div><!-- ROW -->
</section>

<?php get_footer(); ?>
