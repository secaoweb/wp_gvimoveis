<?php get_header(); ?>

<?php if (function_exists('sw_slider_show')) { sw_slider_show(); } ?>

<?php // echo do_shortcode('[property_search]'); ?>
<section id="filtro-home" class="container">
    <div class="row">
        <div class="col-md-12">
            <form action="http://localhost:8888/projects/gvimoveis/imoveis/" method="post">
                <div class="form-group col-md-3">
                    <label class="select">
                        <select class="form-control" name="wpp_search[property_type]">
                            <option selected value="-1" >Tipo do Imóvel</option>
                            <?php
                            $property_types = get_attribute_values('property_type');

                            if (!empty($property_types)) {
                                foreach ($property_types as $property_type) {
                                    echo '<option value="'.$property_type->meta_value.'">
                                            '.toUppercase($property_type->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group col-md-2">
                    <label class="select">
                        <select class="form-control" name="wpp_search[tipo_de_negociao]">
                            <option selected value="-1" >Tipo do Negociação</option>
                            <?php
                            $negociation_types = get_attribute_values('tipo_de_negociao');

                            if (!empty($negociation_types)) {
                                foreach ($negociation_types as $negociation_type) {
                                    echo '<option value="'.$negociation_type->meta_value.'">
                                            '.toUppercase($negociation_type->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group col-md-2">
                    <label class="select">
                        <select class="form-control" name="wpp_search[cidade]">
                            <option selected value="-1" >Cidade</option>
                            <?php
                            $cities = get_attribute_values('cidade');

                            if (!empty($cities)) {
                                foreach ($cities as $city) {
                                    echo '<option value="'.$city->meta_value.'">
                                            '.toUppercase($city->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group col-md-3">
                    <label class="input">
                        <input type="text" class="form-control" name="wpp_search[bairro]" placeholder="Bairro" />
                    </label>
                </div>

                <div class="form-group col-md-2">
                    <button type="submit" class="btn btn-light-blue form-control">BUSCAR</button>
                </div>
            </form>
        </div><!-- col-md-12 -->
    </div><!-- row -->
</section>

<section id="destaques" class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="title-section">Imóveis em destaque</h1>
        </div>
    </div>

    <div class="row">
        <?php echo do_shortcode('[property_overview featured=true hide_count=true pagination=off template="featured-shortcode" per_page=3]'); ?>
    </div>
</section>

<section id="banner-promo" class="container">
    <img src="http://7606-presscdn-0-74.pagely.netdna-cdn.com/wp-content/uploads/2016/03/Dubai-Photos-Images-Oicture-Dubai-Landmarks-800x600.jpg" alt="">
</section>

<section id="imoveis-normais" class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="title-section">Imóveis para Venda</h1>
        </div>
    </div>

    <div class="row">
        <?php echo do_shortcode('[property_overview hide_count=true pagination=off tipo_de_negociao=Venda template="others-shortcode" per_page=4]'); ?>
    </div>
</section>

<section id="imoveis-normais" class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="title-section">Imóveis para Locação</h1>
        </div>
    </div>

    <div class="row">
        <?php echo do_shortcode('[property_overview hide_count=true pagination=off tipo_de_negociao=Locação template="others-shortcode" per_page=4]'); ?>
    </div>
</section>

<?php get_footer(); ?>
