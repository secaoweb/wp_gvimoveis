<?php get_header(); ?>

<section class="breadcrumb-blog">
    <div class="container">
        <div class="pull-left"><h1>Busca</h1></div>
    </div>
</section>

<section class="container" id="body-blog">
    <div class="row">
        <main class="col-md-9">
            <?php get_template_part('loop', 'search'); ?>
            <span class="clearfix"></span>

            <nav aria-label="Page navigation" class="clearfix">
                <?php echo sw_pagination(); ?>
            </nav>
        </main>

        <aside class="col-md-3">
            <?php dynamic_sidebar('main'); ?>
        </aside>
    </div><!-- ROW -->
</section>

<?php get_footer(); ?>
