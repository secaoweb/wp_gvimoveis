<aside id="side-content" class="col-md-3 ">
    <div id="filtro-listagem">
        <h1 class="info">Opções de busca</h1>
        <div class="conteudo-filtro">
            <form action="<?php echo get_site_url(); ?>/imoveis/" method="post">
                <div class="form-group" style="margin-top: 0px;">
                    <label class="label-text">Tipo do Imóvel</label>
                    <label class="select">
                        <select class="form-control" name="wpp_search[property_type]">
                            <option value="-1">Todos os tipos</option>
                            <?php
                            $property_types = get_attribute_values('property_type');

                            if (!empty($property_types)) {
                                foreach ($property_types as $property_type) {
                                    $selected = ($_GET['wpp_search'][property_type] == $property_type->meta_value) ? 'selected' : '';
                                    echo  '<option '.$selected.' value="'.$property_type->meta_value.'">
                                            '.toUppercase($property_type->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text">Tipo de Negociação</label>
                    <label class="select">
                        <select class="form-control" name="wpp_search[tipo_de_negociao]">
                            <option selected value="-1">Todos os tipos</option>
                            <?php
                            $negociation_types = get_attribute_values('tipo_de_negociao');

                            if (!empty($negociation_types)) {
                                foreach ($negociation_types as $negociation_type) {
                                    $selected = ($_GET['wpp_search'][tipo_de_negociao] == $negociation_type->meta_value) ? 'selected' : '';

                                    echo '<option '.$selected.' value="'.$negociation_type->meta_value.'">
                                            '.toUppercase($negociation_type->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text">Cidade</label>
                    <label class="select">
                        <select class="form-control" name="wpp_search[cidade]">
                            <option selected value="-1" >Cidade</option>
                            <?php
                            $cities = get_attribute_values('cidade');

                            if (!empty($cities)) {
                                foreach ($cities as $city) {
                                    $selected = ($_GET['wpp_search'][cidade] == $city->meta_value) ? 'selected' : '';

                                    echo '<option '.$selected.' value="'.$city->meta_value.'">
                                            '.toUppercase($city->meta_value).'
                                        </option>';
                                }
                            }
                            ?>
                        </select>
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text" >Bairro</label>
                    <label class="input">
                        <input type="text" name="wpp_search[bairro]" value="<?php echo $_GET['wpp_search'][bairro] ?>" />
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text" >Quartos</label>
                    <label class="input">
                        <input type="text" name="wpp_search[quartos]" value="<?php echo $_GET['wpp_search'][quartos] ?>" />
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text" >Banheiros</label>
                    <label class="input">
                        <input type="text" name="wpp_search[banheiros]" value="<?php echo $_GET['wpp_search'][banheiros] ?>" />
                    </label>
                </div>

                <div class="form-group">
                    <label class="label-text" >Vagas</label>
                    <label class="input">
                        <input type="text" name="wpp_search[vagas]" value="<?php echo $_GET['wpp_search'][vagas] ?>" />
                    </label>
                </div>

                <div class="form-group">
                    <label>
                        <input type="checkbox" name="wpp_search[piscina]" value="true"> &nbsp;&nbsp;Piscina <br><br>
                        <input type="checkbox" name="wpp_search[mobiliado]" value="true"> &nbsp;&nbsp;Mobiliado
                    </label>
                </div>

                <!-- <div id="form-avancado">
                    <div class="form-group">
                        <label class="label-text" >Tipo de Negociação</label>
                        <label class="select">
                            <select class="form-control">
                                <option selected >Tipo do Bairros</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </label>
                    </div>
                </div> -->

                <!-- <h2 class="busca-avancada">+ Busca Avançada</h2> -->
                <h4 class="text-center"><a href="<?php echo get_site_url(); ?>/imoveis/">Remover filtros</a></h4>
                <button type="submit" class="btn btn-light-blue">Filtrar</button>
            </form>
        </div>
    </div>
</aside>
