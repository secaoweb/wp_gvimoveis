<?php
if (have_posts()):
    while (have_posts()):
        the_post();

        $tags = get_the_tags();
        ?>
        <article class="item-post">
            <div class="image-post">
                <?php the_post_thumbnail(); ?>
            </div>

            <div class="date-post">
                <h1><?php the_time('d'); ?> </h1>
                <h3><?php the_time('F'); ?> </h3>
                <h4><?php the_time('Y'); ?> </h4>
                <div class="border"></div>
            </div>

            <div class="body-post">
                <div class="body-post-title">
                    <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                </div>

                <div class="body-post-info row">
                    <div class="author">
                        <i class="fa fa-pencil" aria-hidden="true"></i> <?php the_author_meta('display_name'); ?>
                    </div>

                    <div class="category-info">
                        <?php
                        echo '<i class="fa fa-folder-open" aria-hidden="true"></i> ';

                        if (!empty($tags)):
                            $count = 1;
                            $qtd_tag = count($tags);

                            foreach ($tags as $tag):
                                $delimiter = ($count < $qtd_tag) ? ", " : "";
                                echo '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>' . $delimiter;

                                $count++;
                            endforeach;
                        else:
                            echo "<span>Nenhuma tag</span>";
                        endif;
                        ?>
                    </div>

                    <!-- <div class="comment-counter">
                        <a href="#"><i class="fa fa-comments" aria-hidden="true"></i> 13</a>
                    </div> -->
                </div><!-- END - BODY-POST-INFO -->

                <div class="visible-sm">
                    <div class="post-content">
                        <p><?php the_excerpt(); ?></p>
                    </div>

                    <div class="links-post">
                        <div class="socials pull-left">
                            <?php social_likes($post_id); ?>

                            <!-- <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
                        </div>
                        <div class="read-more pull-right">
                            <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-blog">Leia Mais</a>
                        </div>
                    </div>
                </div>
            </div><!-- BODY-POST -->

            <div class="hidden-sm">
                <div class="post-content">
                    <p><?php the_excerpt(); ?></p>
                </div>

                <div class="links-post">
                    <div class="socials pull-left">
                        <?php social_likes($post_id); ?>

                        <!-- <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a> -->
                    </div>
                    <div class="read-more pull-right">
                        <a href="<?php the_permalink(); ?>" class="btn btn-primary btn-blog">Leia Mais</a>
                    </div>
                </div>
            </div>
            <span class="clearfix"></span>
        </article><!-- ITEM POST -->

        <?php
    endwhile; // end of the loop.
else:
    ?>

    <article class="item-post">
        <div class="body-post">
            <div class="body-post-title">
                <h2>Nada foi encontrado!</h2>
            </div>

            <div class="post-content">
                <p>
                Nenhum resultado foi encontrado com o termo buscado:
                <strong><?php echo get_search_query(); ?></strong>,
                realize uma nova busca com um novo termo.
            </p>
            </div>
        </div>
    </article><!-- ITEM POST -->

    <?php
endif;
?>
