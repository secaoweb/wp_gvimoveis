<?php
/**
 * WP-Property Featured Shortcode Template
 *
 * @version 1.0
 * @author Adriano Vasconcelos <adriano.vasconcelos@secaoweb.com.br>
 * @package WP-Property
*/

if (have_properties()):
    foreach (returned_properties('load_gallery=false') as $property):
        ?>

        <div class="col-md-3 item">
            <div class="imagem-imovel">
                <div class="image-container">
                    <a href="<?php echo $property['permalink']; ?>">
                        <img src="<?php echo $property['featured_image_url']; ?>" alt="<?php echo $property['title']; ?>">
                    </a>
                </div>
                <div class="overlay-imovel">
                    <div class="area"><?php echo $property['area']; ?></div>
                    <div class="preco"><?php echo $property['price']; ?></div>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="info-imovel">
                <a href="<?php echo $property['permalink']; ?>"><h1><?php echo $property['title']; ?></h1></a>
                <p><?php echo wp_trim_words($property['post_content'], 20); ?></p>
            </div>

            <div class="footer-imovel">
                <span class="fa-stack fa-lg">
                    <i class="fa fa-bed fa-stack-1x"></i>
                </span>
                <?php echo sprintf(_n('%s Quarto', '%s Quartos', $property['quartos']), $property['quartos']); ?>
                <span class="fa-stack fa-lg">
                    <i class="fa fa-bath fa-stack-1x"></i>
                </span>
                <?php echo sprintf(_n('%s Banheiro', '%s Banheiros', $property['banheiros']), $property['banheiros']); ?>
                <span class="fa-stack fa-lg">
                    <i class="fa fa-car fa-stack-1x"></i>
                </span>
                <?php echo sprintf(_n('%s Vaga', '%s Vagas', $property['vagas']), $property['vagas']); ?>
            </div>
        </div><!-- col-md-4 -->

        <?php
    endforeach;
else:
    echo '<h3 style="padding-left: 15px;margin-top: 0px;">Nenhum imóvel nesta área no momento.</h3>';
endif;
