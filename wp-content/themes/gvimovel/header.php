<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, maximum-scale=1">

        <title>
            <?php
            //Print the <title> tag based on what is being viewed.
            global $page, $paged;
            $title = wp_title(' ', false, 'right');

            if (empty($title)) {
                echo get_bloginfo('name').' - '.get_bloginfo('description');
            } else {
                echo trim($title).' - '.get_bloginfo('name');
            }

            // Add a page number if necessary:
            if ($paged >= 2 || $page >= 2)
                echo ' - ' . sprintf('Página %s', max($paged, $page));
            ?>
        </title>

        <link rel="icon" href="<?php echo bloginfo('template_directory') ?>/images/icon.png" type="image/png">
        <link rel="shortcut icon" href="<?php echo bloginfo('template_directory') ?>/images/icon.png" type="image/png">

        <link href="<?php echo bloginfo('template_directory') ?>/style.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css">
        <link rel="stylesheet" href="<?php echo bloginfo('template_directory') ?>/styles/styles.css">
        <style>#map-footer{width: 100%; height: 170px;}</style>

        <?php wp_head(); ?>
    </head>

    <body>
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="<?php echo home_url(); ?>">
                        <img src="<?php echo bloginfo('template_directory') ?>/images/logo.png" alt="GV Imóvel" />
                    </a>
                </div>

                <?php echo sw_nav_menu('Principal'); ?>
            </div><!-- container-fluid-->
        </nav>
