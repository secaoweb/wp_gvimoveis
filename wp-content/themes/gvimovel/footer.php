    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-3 logo-footer">
                    <a href="<?php echo home_url(); ?>"><img src="<?php echo bloginfo('template_directory'); ?>/images/logo.png" alt="<?php wp_title('|', true, 'right'); ?>"></a>
                </div>
                <div class="col-md-3">
                    <div class="header-menu">
                        Contato
                    </div>
                    <ul>
                        <li>Email: <a href="mailto:<?php echo gv_get_theme_option('email'); ?>"><?php echo gv_get_theme_option('email'); ?></a></li>
                        <li>Telefone: <?php echo gv_get_theme_option('telefone'); ?></li>
                        <li>Whatsapp: <?php echo gv_get_theme_option('whatsapp'); ?></li>
                        <li>Endereço: Rua almirante rubim 1726, Montese, Fortaleza/CE</li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <div class="header-menu">
                        LOCALIZAÇÃO
                    </div>
                    <div class="maps" id="map-footer">

                    </div>
                </div>
                <div class="col-md-3">
                    <div class="header-menu">
                        info
                    </div>
                    <ul>
                        <li><a href="">Como posso vender?</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.js" charset="utf-8"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1pdpKuynY2V5cRqaP4erMYwR3Llos1Mk"></script>
    <script src="<?php echo bloginfo('template_directory') ?>/scripts/custom-maps.js"></script>
    <script src="<?php echo bloginfo('template_directory') ?>/scripts/app.js"></script>
    <script src="<?php echo bloginfo('template_directory') ?>/scripts/jquery.blockUI.js"></script>
    <script src="<?php echo bloginfo('template_directory') ?>/scripts/app.pagination.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>
    <script src="<?php echo bloginfo('template_directory') ?>/scripts/app.dynamic.sidebar.js"></script>

</body>
</html>
