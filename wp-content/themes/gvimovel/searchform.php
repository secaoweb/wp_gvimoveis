<div id="busca" class="form-group row">
    <div class="col-md-12">
        <form class="searchform" method="get" action="<?php echo esc_url(home_url('/')); ?>">
            <div class="input-group">
                <input type="text" class="form-control" id="s2" name="s" placeholder="Buscar" aria-describedby="search-aside">
                <span class="input-group-addon" id="search-aside"><i class="fa fa-search"></i></span>
            </div>
        </form>
    </div>
</div><!-- BUSCA -->
