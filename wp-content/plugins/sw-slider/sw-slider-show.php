<?php

/* mostra o slide no site */
function sw_slider_show() {
    global $wpdb;

    if (function_exists('plugins_url')):
        $url = plugins_url(plugin_basename(dirname(__FILE__)));
    else:
        $url = get_option('siteurl') . '/wp-content/plugins/' . plugin_basename(dirname(__FILE__));
    endif;

    $slider_plugin_dir = ABSPATH . 'wp-content/plugins/sw-slider/';
    $slider_plugin_url = $url;
    $slider_files_dir = ABSPATH . 'wp-content/uploads/sw_slider_files/';
    $slider_files_url = get_option('siteurl') . '/wp-content/uploads/sw_slider_files/';

    $query = "SELECT slider_id, slider_title, slider_subtitle, slider_preco, slider_area, slider_button_label, slider_image_link, slider_image_type, slider_image_status
        FROM {$wpdb->prefix}sw_slider
        WHERE slider_image_status = 1
        OR slider_image_status IS NULL
        ORDER BY slider_order, slider_id";

    $items = $wpdb->get_results($query);
    ?>

    <div id="banner-home" class="container">
        <?php foreach ($items as $item): ?>

            <div class="item-slide-home" style="background: url('<?php echo $slider_files_url.$item->slider_id.'_sw.'.$item->slider_image_type; ?>') no-repeat center center">
                <div class="col-xs-8 col-xs-offset-1 col-sm-5 col-sm-offset-1 col-md-4 col-md-offset-1">
                    <div class="card-venda">
                        <?php if (!empty($item->slider_title)): ?>
                            <h1><?php echo $item->slider_title; ?></h1>
                        <?php endif; ?>

                        <?php if (!empty($item->slider_subtitle)): ?>
                            <p><?php echo $item->slider_subtitle; ?></p>
                        <?php endif; ?>

                        <?php if (!empty($item->slider_preco)): ?><span class="preco-card"><?php echo $item->slider_preco; ?></span><?php endif; ?>
                        <?php if (!empty($item->slider_area)): ?><span class="area-card"><?php echo $item->slider_area; ?></span><?php endif; ?>
                    </div>
                    <?php if (!empty($item->slider_image_link)): ?>
                        <a href="<?php echo $item->slider_image_link; ?>" class="btn btn-card" target="_blank">
                            <?php echo ($item->slider_button_label) ? $item->slider_button_label : 'CONFIRA AGORA!'; ?>
                        </a>
                    <?php endif; ?>
                </div>
            </div>

        <?php endforeach; ?>
    </div>

    <?php
}
?>
