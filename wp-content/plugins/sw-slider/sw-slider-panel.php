<?php

global $wpdb;
global $db_name;
$db_name = $wpdb->prefix.'sw_slider';
$wpdb->show_errors();

if (function_exists('plugins_url')) {
    $url = plugins_url(plugin_basename(dirname(__FILE__)));
} else {
    $url = get_option('siteurl') . '/wp-content/plugins/' . plugin_basename(dirname(__FILE__));
}

$slider_plugin_dir = ABSPATH . 'wp-content/plugins/sw-slider/';
$slider_plugin_url = $url;
$slider_files_dir = ABSPATH . 'wp-content/uploads/sw_slider_files/';
$slider_files_url = get_option('siteurl') . '/wp-content/uploads/sw_slider_files/';

if (!empty($_POST)) {
    $slider_title = (!empty($_POST['slider_title'])) ? $_POST['slider_title'] : null;
    $slider_subtitle = (!empty($_POST['slider_subtitle'])) ? $_POST['slider_subtitle'] : null;
    $slider_preco = (!empty($_POST['slider_preco'])) ? $_POST['slider_preco'] : null;
    $slider_area = (!empty($_POST['slider_area'])) ? $_POST['slider_area'] : null;
    $slider_button_label = (!empty($_POST['slider_button_label'])) ? $_POST['slider_button_label'] : null;
    $slider_image_link = (!empty($_POST['slider_image_link'])) ? $_POST['slider_image_link'] : null;

    $types = ['%d', '%s', '%s', '%s', '%s', '%s', '%s', '%d'];
    $values = [
        'slider_order' => $_POST['slider_order'],
        'slider_title' => $slider_title,
        'slider_subtitle' => $slider_subtitle,
        'slider_preco' => $slider_preco,
        'slider_area' => $slider_area,
        'slider_button_label' => $slider_button_label,
        'slider_image_link' => $slider_image_link,
        'slider_image_status' => 1
    ];

    if (isset($_FILES['file']) && !empty($_FILES['file']['tmp_name'])) {
        $_UP['error'] = false;

        // Pasta onde o arquivo vai ser salvo
        $_UP['directory'] = $slider_files_dir;
        // Tamanho máximo do arquivo (em Bytes)
        $_UP['size'] = 1024 * 1024 * 5; // 5Mb
        // Array com as extensões permitidas
        $_UP['extensions'] = array('jpg', 'png', 'gif');

        // Array com os tipos de erros de upload do PHP
        $_UP['erros'][0] = 'Não houve erro.';
        $_UP['erros'][1] = 'O arquivo no upload é maior do que o limite do PHP.';
        $_UP['erros'][2] = 'O arquivo ultrapassa o limite de tamanho especifiado no HTML.';
        $_UP['erros'][3] = 'O upload do arquivo foi feito parcialmente.';

        // Verifica se houve algum erro com o upload. Se sim, exibe a mensagem do erro
        if ($_FILES['file']['error'] != 0) {
            echo "Não foi possível fazer o upload, erro:" . $_UP['erros'][$_FILES['file']['error']];
        } else {
            // Faz a verificação da extensão do arquivo
            $extension = strtolower(end(explode('.', $_FILES['file']['name'])));
            if (array_search($extension, $_UP['extensions']) === false) {
                $_UP['error'] = true;
                echo "Por favor, envie arquivos com as seguintes extensões: jpg, png ou gif";
            }

            // Faz a verificação do tamanho do arquivo
            if ($_UP['size'] < $_FILES['file']['size']) {
                $_UP['error'] = true;
                echo "O arquivo enviado é muito grande, envie arquivos de até 2Mb.";
            }

            if (!$_UP['error']) {
                $slider_last_id = ($_POST['slider_id']) ? $_POST['slider_id'] : $wpdb->get_var("SELECT slider_id + 1 FROM {$db_name} ORDER BY slider_id DESC LIMIT 1");
                if (!$slider_last_id) {
                    $slider_last_id = 1;
                }

                $name_file = $slider_last_id.'_sw.'.$extension;
                $upload_file = move_uploaded_file($_FILES['file']['tmp_name'], $_UP['directory'].$name_file);

                // Upload efetuado com sucesso
                if ($upload_file) {
                    $types[] = '%s';
                    $values['slider_image_type'] = $extension;
                } else {
                    echo "Não foi possível enviar o arquivo, tente novamente mais tarde ou entre em contato com o suporte.";
                }
            }
        }
    }

    if(isset($_POST['slider_id']) && !empty($_POST['slider_id'])) {
        $wpdb->update($db_name, $values, ['slider_id' => intval($_POST['slider_id'])], $types, ['%d']);
    } else {
        $wpdb->insert($db_name, $values, $types);
    }
}

if (isset($_GET['edit'])) {
    $sw_slider_file = $wpdb->get_row("SELECT * FROM {$db_name} WHERE slider_id = '$_GET[edit]'");
    unset($_GET);
}

if (isset($_GET['remove'])) {
    $sw_slider_file_type = $wpdb->get_var("SELECT slider_image_type FROM {$db_name} WHERE slider_id = '$_GET[remove]'");
    $wpdb->query("DELETE FROM {$db_name} WHERE slider_id = $_GET[remove]");

    $sw_image_file = $slider_files_dir.$_GET['remove'].'_sw.'.$sw_slider_file_type;
    if (is_file($sw_image_file)) {
        unlink($sw_image_file);
    }

    unset($_GET);
}

if (isset($_GET['disable'])) {
    $wpdb->query("UPDATE {$db_name} SET slider_image_status = 0 WHERE slider_id = $_GET[disable]");
    unset($_GET);
}

if (isset($_GET['enable'])) {
    $wpdb->query("UPDATE {$db_name} SET slider_image_status = 1 WHERE slider_id = $_GET[enable]");
    unset($_GET);
}
?>
<style type="text/css">
    textarea, input[type="text"] { width: 100%; }
</style>

<div class="wrap">
    <h2>SW Slider</h2>
    <hr class="wp-header-end">

    <div class="postbox" style="margin-top: 10px;">
        <h2 class="hndle" style="margin: 0; padding: 10px;"><span>Adicionar/Editar Imagem</span></h2>

        <div class="inside">
            <form name="add_image" method="post" action="admin.php?page=sw-slider/sw-slider.php" enctype="multipart/form-data">
                <input type="hidden" name="slider_id" value="<?php if ($sw_slider_file->slider_id) { echo $sw_slider_file->slider_id; } ?>" />

                <table class="form-table">
                    <tbody>
                        <tr>
                            <th scope="row"><label for="file">Imagem:</label></th>
                            <td><input type="file" name="file" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_order">Ordem:</label></th>
                            <td><input type="text" name="slider_order" id="slider_order" value="<?php echo ($sw_slider_file->slider_order) ? $sw_slider_file->slider_order : '0' ?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_title">Título:</label></th>
                            <td><input type="text" name="slider_title" id="slider_title" value="<?php if ($sw_slider_file->slider_title) { echo $sw_slider_file->slider_title; } ?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_subtitle">Subtítulo:</label></th>
                            <td><textarea name="slider_subtitle" id="slider_subtitle"><?php if ($sw_slider_file->slider_subtitle) { echo $sw_slider_file->slider_subtitle; } ?></textarea></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_preco">Preço:</label></th>
                            <td><input type="text" name="slider_preco" id="slider_preco" value="<?php if ($sw_slider_file->slider_preco) { echo $sw_slider_file->slider_preco; } ?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_area">Área:</label></th>
                            <td><input type="text" name="slider_area" id="slider_area" value="<?php if ($sw_slider_file->slider_area) { echo $sw_slider_file->slider_area; } ?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_button_label">Texto Botão:</label></th>
                            <td><input type="text" name="slider_button_label" id="slider_button_label" value="<?php if ($sw_slider_file->slider_button_label) { echo $sw_slider_file->slider_button_label; } ?>" /></td>
                        </tr>
                        <tr>
                            <th scope="row"><label for="slider_image_link">Link:</label></th>
                            <td><input type="text" name="slider_image_link" id="slider_image_link" placeholder="http://dominio.com.br" value="<?php if ($sw_slider_file->slider_image_link) { echo $sw_slider_file->slider_image_link; } ?>" /></td>
                        </tr>
                    </tbody>
                </table>

                <p class="submit"><input type="submit" value="Salvar" class="button-primary action"/></p>
            </form>
        </div>
    </div>

    <div id="slider_images">
        <table class="widefat">
            <thead>
                <tr>
                    <th>Ordem</th>
                    <th>Imagem</th>
                    <th>Título</th>
                    <th>Subtítulo</th>
                    <th>Preço</th>
                    <th>Área</th>
                    <th>Link</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <th>Ordem</th>
                    <th>Imagem</th>
                    <th>Título</th>
                    <th>Subtítulo</th>
                    <th>Preço</th>
                    <th>Área</th>
                    <th>Link</th>
                    <th>Ações</th>
                </tr>
            </tfoot>
            <tbody>
                <?php
                $items = $wpdb->get_results("SELECT * FROM {$db_name} ORDER BY slider_order, slider_id ASC");

                if ($items) {
                    foreach ($items as $item) {
                        ?>

                        <tr>
                            <td><?php echo $item->slider_order; ?></td>
                            <td width="15%">
                                <img width="80%" src="<?php echo $slider_files_url.$item->slider_id.'_sw.'.$item->slider_image_type; ?>" />
                            </td>
                            <td><?php if (!empty($item->slider_title)) { echo stripslashes($item->slider_title); } ?></td>
                            <td><?php if (!empty($item->slider_subtitle)) { echo stripslashes($item->slider_subtitle); } ?></td>
                            <td><?php if (!empty($item->slider_preco)) { echo stripslashes($item->slider_preco); } ?></td>
                            <td><?php if (!empty($item->slider_area)) { echo stripslashes($item->slider_area); } ?></td>
                            <td width="10%"><?php if (!empty($item->slider_image_link)) { echo '<a href="'.stripslashes($item->slider_image_link).'" target="_blank">'.$item->slider_button_label.'</a>'; } ?></td>
                            <td width="8%">
                                <small>
                                    <a href="admin.php?page=sw-slider/sw-slider.php&edit=<?php echo $item->slider_id; ?>">
                                        <img src="<?php echo $slider_plugin_url."/img/edit.png" ?>" alt="Editar" title="Editar" />
                                    </a>
                                    <a href="admin.php?page=sw-slider/sw-slider.php&remove=<?php echo $item->slider_id; ?>">
                                        <img src="<?php echo $slider_plugin_url."/img/remove.png" ?>" alt="Remover" title="Remover" />
                                    </a>

                                    <?php if ($item->slider_image_status): ?>
                                        <a href="admin.php?page=sw-slider/sw-slider.php&disable=<?php echo $item->slider_id; ?>">
                                            <img src="<?php echo $slider_plugin_url."/img/disable.png" ?>" alt="Desabilitar" title="Desabilitar" />
                                        </a>
                                    <?php else: ?>
                                        <a href="admin.php?page=sw-slider/sw-slider.php&enable=<?php echo $item->slider_id; ?>">
                                            <img src="<?php echo $slider_plugin_url."/img/enable.png" ?>" alt="Habilitar" title="Habilitar" />
                                        </a>
                                    <?php endif; ?>
                                </small>
                            </td>
                        </tr>

                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
