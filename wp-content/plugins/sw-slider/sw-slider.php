<?php

/*
  Plugin Name: SW Slider
  Description: Plugin SW Slider
  Version: 1.0.0
  Author: Seção Web
  Author URI: http://www.secaoweb.com.br/
 */

global $slider_plugindir;
$slider_plugindir = ABSPATH.'wp-content/plugins/sw-slider/';

add_action('admin_menu', 'sw_slider_install');
function sw_slider_install() {
    $sw_slider_files = ABSPATH.'wp-content/uploads/sw_slider_files';
    global $wpdb;

    /* Cria a pasta onde ficarão as images do upload */
    if (!file_exists($sw_slider_files)) {
        umask(0);
        mkdir($sw_slider_files, 0777, true) or die('Error creating the folder '.$sw_slider_files .'check folder permissions');
    }

    /* adiciona menu e submenus */
    add_menu_page('SW Slider', 'SW Slider', 'read', __FILE__, 'sw_slider_panel', 'dashicons-images-alt2', 3);
    add_submenu_page(__FILE__, 'Add/Editar imagem', 'Add/Editar imagem', 'read', 'sw-slider/sw-slider.php');

    /* cria tabela no banco de dados */
    $query = "CREATE TABLE IF NOT EXISTS `{$wpdb->prefix}sw_slider` (
            `slider_id` INT NOT NULL AUTO_INCREMENT,
            `slider_order` INT,
            `slider_title` TEXT,
            `slider_subtitle` TEXT,
            `slider_preco` VARCHAR(100),
            `slider_area` VARCHAR(100),
            `slider_button_label` VARCHAR(25),
            `slider_image_type` VARCHAR(10),
            `slider_image_link` VARCHAR(255),
            `slider_image_status` TINYINT(1),
            PRIMARY KEY (`slider_id`)
        );";

    $wpdb->query($query);
}

function sw_slider_panel() {
    include 'sw-slider-panel.php';
}

require_once($slider_plugindir.'sw-slider-show.php');
